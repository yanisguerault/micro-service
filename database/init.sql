
CREATE DATABASE IF NOT EXISTS `auth` DEFAULT CHARACTER SET utf8mb4;
USE `auth`;

CREATE TABLE IF NOT EXISTS `user` (
                                      `username` varchar(50) NOT NULL,
                                      `password` varchar(500) NOT NULL,
                                      PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `user` DISABLE KEYS;
INSERT INTO `user` (`username`, `password`) VALUES
('ginette@gmail.com', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8'),
('wall-e@esiee.fr', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8'),
('yanisguerault@gmail.com', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8');
ALTER TABLE `user` ENABLE KEYS;


CREATE DATABASE IF NOT EXISTS `friends` DEFAULT CHARACTER SET utf8mb4;
USE `friends`;

CREATE TABLE IF NOT EXISTS `friend` (
                                        `friend1` int(11) NOT NULL,
                                        `friend2` int(11) NOT NULL,
                                        `dateFriendship` date NOT NULL,
                                        PRIMARY KEY (`friend1`,`friend2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `friend` DISABLE KEYS ;
INSERT INTO `friend` (`friend1`, `friend2`, `dateFriendship`) VALUES
(1, 3, '2021-02-20'),
(3, 1, '2021-02-16'),
(3, 4, '2021-02-20'),
(4, 1, '2021-02-20'),
(4, 3, '2021-02-20');
ALTER TABLE `friend` ENABLE KEYS ;


CREATE DATABASE IF NOT EXISTS `messaging` DEFAULT CHARACTER SET utf8mb4 ;
USE `messaging`;

CREATE TABLE IF NOT EXISTS `conversation` (
                                              `id` int(11) NOT NULL AUTO_INCREMENT,
                                              `user1` int(11) NOT NULL,
                                              `user2` int(11) NOT NULL,
                                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

ALTER TABLE `conversation` DISABLE KEYS ;
INSERT INTO `conversation` (`id`, `user1`, `user2`) VALUES
(1, 3, 1),
(2, 1, 3),
(3, 4, 1),
(4, 4, 3),
(5, 3, 4);
ALTER TABLE `conversation` ENABLE KEYS ;

CREATE TABLE IF NOT EXISTS `message` (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `idConversation` int(11) DEFAULT NULL,
                                         `userSending` int(11) NOT NULL,
                                         `message` text DEFAULT NULL,
                                         `dateSending` datetime NOT NULL,
                                         PRIMARY KEY (`id`),
                                         KEY `FK_conversation_message` (`idConversation`),
                                         CONSTRAINT `FK_conversation_message` FOREIGN KEY (`idConversation`) REFERENCES `conversation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

ALTER TABLE `message` DISABLE KEYS ;
INSERT INTO `message` (`id`, `idConversation`, `userSending`, `message`, `dateSending`) VALUES
(1, 1, 3, 'Salut Yanis\n', '2021-02-16 10:55:57'),
(2, 2, 1, 'Salut Wall-E\n', '2021-02-20 10:04:35'),
(3, 2, 1, 'Ca va ?\n', '2021-02-20 10:04:45'),
(4, 2, 1, 'Salut\n', '2021-02-20 10:09:15'),
(5, 2, 1, 'Hello\n', '2021-02-20 10:17:39'),
(6, 1, 3, 'Salut\n', '2021-02-20 10:18:28'),
(7, 2, 1, 'Bonjour\n', '2021-02-20 11:08:09'),
(8, 3, 4, 'Bonjour jeune homme\n', '2021-02-20 11:09:14'),
(9, 4, 4, ' Bonjour jeune homme\n', '2021-02-20 11:12:50'),
(10, 5, 3, 'Bonjour Madame\n', '2021-02-20 11:12:55'),
(11, 5, 3, 'Comment allez-vous ?\n', '2021-02-20 11:13:27'),
(12, 4, 4, 'Bien et toi ?\n', '2021-02-20 11:13:31'),
(13, 5, 3, 'Fort bien\n', '2021-02-20 11:13:34');
ALTER TABLE `message` ENABLE KEYS ;


CREATE DATABASE IF NOT EXISTS `profiles` DEFAULT CHARACTER SET utf8mb4 ;
USE `profiles`;

CREATE TABLE IF NOT EXISTS `profile` (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `email` varchar(100) NOT NULL DEFAULT '',
                                         `name` varchar(100) NOT NULL DEFAULT '',
                                         `firstname` varchar(100) NOT NULL DEFAULT '',
                                         `gender` varchar(10) NOT NULL DEFAULT '',
                                         `birthday` date NOT NULL DEFAULT '0000-00-00',
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

ALTER TABLE `profile` DISABLE KEYS ;
INSERT INTO `profile` (`id`, `email`, `name`, `firstname`, `gender`, `birthday`) VALUES
(1, 'yanisguerault@gmail.com', 'Guerault', 'Yanis', 'Male', '1998-12-10'),
(3, 'wall-e@esiee.fr', '-E', 'Wall', 'Male', '1994-01-01'),
(4, 'ginette@gmail.com', 'Ginette', 'Madame', 'Female', '1935-05-15');
ALTER TABLE `profile` ENABLE KEYS ;
