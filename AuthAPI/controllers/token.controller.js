var jwt = require('jsonwebtoken');
const envConf = require("../config/env.config");
const axios = require('axios');
const Auth = require("../models/auth.model");

exports.verifyToken = (req, res, next, result) => {

    jwt.verify(req.cookies['X-Auth-Token'], envConf.JWT_KEY, (err, decoded) => {

        if (!err) {

            Auth.getUser(decoded.id, (err, value) => {

                if (!err) {
                    result(true);
                } else {
                    result(false);
                }

            })
        } else {

            result(false)

        }
    })
};

exports.addToken = (username, result) => {

    axios.post("http://" + process.env.PROFILE_API + "/profile/getProfile",
        {
            id: username
        }, {withCredentials: true, headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}})
        .then(response => {

            var personalToken = jwt.sign({
                id: username,
                profileId: response.data.id,
                data: Date.now()
            }, envConf.JWT_KEY);

            result(personalToken)

        })
        .catch(e => {

            console.log(e)
            result(null)

        })
};