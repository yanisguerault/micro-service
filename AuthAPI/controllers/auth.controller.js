/*
    Import
 */
const Auth = require("../models/auth.model");
const Token = require("./token.controller");
const crypto = require('crypto');
const axios = require('axios');

exports.verifyAuthentification = (req, res, next, result) => {

    if (req.body.password == undefined || req.body.username == undefined) {
        result(false)
        return;
    }

    var sha512 = crypto.createHash('sha512');
    var data = sha512.update(req.body.password, 'utf-8');
    var password = data.digest('hex');

    Auth.verifyAuthentification(req.body.username, password, (err, data) => {

        if (err) {
            result(false, null)

        } else {

            Token.addToken(req.body.username, (token) => {
                result(true, token)

            })
        }
    });
};

exports.createUser = (req, result) => {

    if (req.body.password == undefined || req.body.username == undefined) {
        result(false, null)
        return;
    }

    var sha512 = crypto.createHash('sha512');
    var data = sha512.update(req.body.password, 'utf-8');
    var hashPassword = data.digest('hex');

    const newUser = new Auth({
        username: req.body.username,
        password: hashPassword
    });

    Auth.createUser(newUser, (err) => {
        if (!err) {

            axios.post("http://" + process.env.PROFILE_API + "/profile/addProfile",
                {
                    username: req.body.username,
                    name: req.body.name,
                    firstname: req.body.firstname,
                    gender: req.body.gender,
                    birthdate: req.body.birthdate
                },{withCredentials: true, headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}})
                .then(response => {

                    if (response.data != "KO") {

                        Token.addToken(req.body.username, (token) => {
                            result(true, token)
                            return;
                        })

                    }

                })
                .catch(e => {
                    console.log(e)
                    result(false, null)
                })
        }
    });
};