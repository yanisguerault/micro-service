const sql = require("./db.model");

// constructor
const Auth = function (auth) {
    this.username = auth.username;
    this.password = auth.password;
};

Auth.getUser = (username, result) => {
    sql.query(`SELECT * FROM user WHERE username = '${username}'`, (err, res) => {

        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res[0]);
            return;
        }

        result({kind: "not_found"}, false);
    });
};

Auth.verifyAuthentification = (username, password, result) => {
    sql.query(`SELECT * FROM user WHERE username = '${username}' AND password='${password}'`, (err, res) => {

        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res[0]);
            return;
        }

        result({kind: "not_found"}, false);
    });
};

Auth.createUser = (newUser, result) => {

    sql.query("INSERT INTO user SET ?", newUser, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err)
            return;
        }
        result(null);
    });

};

module.exports = Auth;