module.exports = app => {
    const Auth = require("../controllers/auth.controller");
    const Token = require("../controllers/token.controller");
    const axios = require('axios');

    app.all("/auth", (req, res, next) => {
        Token.verifyToken(req, res, next, (result) =>
        {

            if (!result) {
                res.status(403).json("KO FROM AUTH");
            } else {
                res.status(200).send("OK")
            }

        });
    });

    app.post("/auth/verifyAuth", (req, res, next) => {
       Auth.verifyAuthentification(req,res,next, (value, token) => {

           if(value)
           {
               res.cookie('X-Auth-Token', token);
               res.json("OK");
           }
           else
           {
               res.json("KO FROM AUTH /verifyAuth");
               console.log("KO FROM AUTH /verifyAuth")
           }

       });
    });

    app.post("/auth/createUser", (req, res, next) => {

        Auth.createUser(req, (value, token) => {
            if(value) {
                res.cookie('X-Auth-Token', token);
                res.json("OK")
            } else
            {
                res.send("KO FROM AUTH /createUser")
                console.log("KO FROM AUTH /createUser")
            }

        });
    });

};