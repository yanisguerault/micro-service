import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { store } from './store/index.js'
import router from './store/router.js'
import VueCookies from 'vue-cookies'
import AxiosPlugin from 'vue-axios-cors';
import axios from 'axios'
import VueAxios from 'vue-axios'
import moment from 'moment'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueCookies)
Vue.use(VueAxios, axios)
Vue.config.productionTip = false

Vue.prototype.basePath = '/'
Vue.prototype.axiosConfig = {
  withCredentials: true,
  headers : {'Content-Type': 'application/json', 'Accept' : 'application/json' }};

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm')
  }
});

Vue.filter('birthdate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
});

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
