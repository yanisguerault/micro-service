import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    form: {
        email: '',
        password: '',
        name: '',
    },

    conv :  [ {id : 'Benjamin'}, 
              {id : 'Juliette'}, 
              {id : 'John'}] ,
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
