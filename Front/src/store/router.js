import { LayoutPlugin } from 'bootstrap-vue'
import Vue from 'vue'
import Router from 'vue-router'
import Index from '../components/Index.vue'
import Login from '../components/Login.vue'
import Signin from '../components/Sigin.vue'
import Connected from '../components/Connected.vue'
import Profil from '../components/Profil.vue'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'home',
        component : Index,
    },
    {
        path: '/Login',
        name: 'login',
        component: Login,
    },
    {
        path: '/Signin',
        name: 'signin',
        component: Signin,
    },
    {
        path: '/HomePage',
        name: 'homepage',
        component: Connected,
    },
    {
        path: '/Profile',
        name: 'profile',
        component: Profil,
    }
    ]
})