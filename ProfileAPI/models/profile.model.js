const sql = require("./db.model");

// constructor
const Profile = function (profile) {
    this.id = profile.id;
    this.email = profile.email;
    this.name = profile.name;
    this.firstname = profile.firstname;
    this.gender = profile.gender;
    this.birthday = profile.birthday;
};

const ProfileWithoutId = function (profile) {
    this.email = profile.email;
    this.name = profile.name;
    this.firstname = profile.firstname;
    this.gender = profile.gender;
    this.birthday = profile.birthday;
};

Profile.getProfiles = (result) => {

    sql.query(`SELECT * FROM profile`, (err, res) => {

        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res);
            return;
        }

        // not found Customer with the id
        result({kind: "not_found"}, null);

    })
}

Profile.getProfile = (idProfil, result) => {

    sql.query(`SELECT * FROM profile WHERE email = '${idProfil}'`, (err, res) => {

        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({kind: "not_found"}, false);

    })
}

Profile.getProfileById = (idProfil, result) => {

    sql.query(`SELECT * FROM profile WHERE id = '${idProfil}'`, (err, res) => {

        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({kind: "not_found"}, false);

    })
}

Profile.addProfile = (profile, result) => {

    profileWId = new ProfileWithoutId(profile);

    sql.query(`INSERT INTO profile SET ?`, profileWId, (err, res) => {

        if (err) {
            console.log("error: ", err);
            result(err);
            return;
        }

        result(null);

    })
}

module.exports = Profile;