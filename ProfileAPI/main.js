const express = require("express");
const bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var cors = require('cors');

const app = express();


/**
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Middlewares for security
 */

// parse requests of content-type: application/json
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(cookieParser());
app.use(cors({ origin: 'http://localhost' , credentials :  true}));

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const route = require("./routes/profile.routes")(app);

// set port, listen for requests
app.listen(80, () => {
    console.log("Server is running on port 80.");
});