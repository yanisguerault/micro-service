module.exports = app => {
    const Profile = require("../controllers/profile.controller");
    var jwt = require('jsonwebtoken');

    app.post("/profile/addProfile", (req, res, next) => {

        Profile.addProfile(req, res, next, (result) =>
        {
            if(result)
            {
                res.json("OK")
            }
            else
            {
                res.json("KO FROM PROFILE /addProfile")
                console.log("KO FROM PROFILE /addProfile")
            }
        });
    });

    app.post("/profile/getProfile", (req, res, next) => {

        Profile.getProfile(req, res, next, (result, data) =>
        {
            if(result)
            {
                res.json(data);
            }
            else
            {
                res.json("KO FROM PROFILE /getProfile")
                console.log("KO FROM PROFILE /getProfile")
            }
        });
    });

    app.post("/profile/getNoFriendsProfile", (req, res, next) => {

        Profile.getNoFriendsProfile(req, res, next, (result, data) =>
        {
            if(result)
            {
                res.json(data);
            }
            else
            {
                res.json("KO FROM PROFILE /getNoFriendsProfile")
                console.log("KO FROM PROFILE /getNoFriendsProfile")
            }
        });
    });

};