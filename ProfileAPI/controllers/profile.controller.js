const Profile = require("../models/profile.model");
var jwt = require('jsonwebtoken');
const envConf = require("../config/env.config");

exports.getProfile = (req, res, next, result) => {

    var profileId;
    var getById = req.body.getById;

    if (req.body.id != undefined) {
        profileId = req.body.id;
    } else {
        jwt.verify(req.cookies['X-Auth-Token'], envConf.JWT_KEY, (err, decoded) => {
            profileId = decoded.profileId;
            getById = true;
        })
    }

    if (profileId == undefined) {
        result(false, null)
        return;
    }

    if (getById) {
        Profile.getProfileById(profileId, (err, data) => {

            if (err) {
                result(false, null)
                return;
            }

            result(true, data != undefined ? data : "[]")
        });
    } else {

        Profile.getProfile(profileId, (err, data) => {

            if (err) {
                result(false, null)
                return;
            }

            result(true, data != undefined ? data : "[]")
        });

    }
};

exports.getNoFriendsProfile = (req,res, next, result) => {

    var friendsArray = req.body.data.map(a => a.friend2);

    var entries = [];

    Profile.getProfiles((err, data) => {

        data.forEach((element, index) => {

            if(!(friendsArray.includes(element.id) || element.id === req.body.idProfile))
            {
                entries.push(element);
            }

            if(index === data.length-1)
            {
                result(true, entries);
            }
        })
    })

}


exports.addProfile = (req, res, next, result) => {

    if (req.body.username == undefined || req.body.name == undefined || req.body.firstname == undefined || req.body.gender == undefined || req.body.birthdate == undefined) {
        result(false)
        return;
    }

    const profile = new Profile({
        email: req.body.username,
        name: req.body.name,
        firstname: req.body.firstname,
        gender: req.body.gender,
        birthday: req.body.birthdate
    })

    Profile.addProfile(profile, (err) => {
        if (err) {
            result(false)
        } else {
            result(true)
        }

    });
};