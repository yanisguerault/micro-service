const Message = require("../models/message.model");
const Conversation = require("../models/conversation.model");

exports.getList = (req, res, next, result) => {

    var user1 = req.body.user1;
    var user2 = req.body.user2;

    if (user1 == undefined || user2 == undefined) {
        result(false, null)
        return;
    }

    Message.getList(user1, user2, (err, data) => {

        if (err) {
            result(false, null)
        } else if (data.length) {
            result(true, data)
        } else {
            result(true, [])
        }

    });
};

exports.addMessage = (req, res, next, result) => {

    var user1 = req.body.user1;
    var user2 = req.body.user2;
    var message = req.body.message;

    if (user1 === undefined || user2 === undefined) {
        result(false)
        return;
    }

    Conversation.getConversationId(user1, user2, (err, data) => {

        if (err || data == null) {
            const conversation = new Conversation({
                user1: user1,
                user2: user2
            });

            Conversation.addConversation(conversation, (err, idConv) => {

                if (err) {
                    result(false, null);
                    return;
                }

                callBackAddMessage(req, res, idConv, result);

            })

        } else {

            callBackAddMessage(req, res, data.id, result);

        }
    })
};

callBackAddMessage = (req, res, idConv, result) => {

    const myMessage = new Message({
        idConversation: idConv,
        userSending: req.body.user1,
        message: req.body.message,
        dateSending: require('moment')(Date.now()).format('YYYY-MM-DD HH:mm:ss')
    })


    Message.addMessage(myMessage, (err) => {
        if (err) {
            result(false)
        } else {
            result(true)
        }
    });

}