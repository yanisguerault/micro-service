// The database parameters for the connection
module.exports = {
    HOST: process.env.DATABASE_URL,
    USER: process.env.DATABASE_USER,
    PASSWORD: process.env.DATABASE_PASSWORD,
    DB: process.env.DATABASE_NAME,
    JWT_KEY: process.env.JWT_KEY
};
