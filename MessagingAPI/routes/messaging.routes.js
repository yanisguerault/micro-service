module.exports = app => {
    const Message = require("../controllers/messaging.controller");

    app.post("/messaging/addMessage", (req, res, next) => {

        Message.addMessage(req, res, next, (result) => {
            if (result) {
                res.json("OK")
            } else {
                res.json("KO FROM MESSAGING /addMessage")
                console.log("KO FROM MESSAGING /addMessage")
            }
        });
    });

    app.post("/messaging/getMessages", (req, res, next) => {

        Message.getList(req, res, next, (result, data) => {
            if (result) {
                res.json(data);
            } else {
                res.json("KO FROM MESSAGING /getMessages")
                console.log("KO FROM MESSAGING /getMessages")
            }
        });
    });

};