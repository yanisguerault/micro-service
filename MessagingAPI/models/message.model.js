const sql = require("./db.model");

// constructor
const Message = function (message) {
    this.idConversation = message.idConversation;
    this.userSending = message.userSending;
    this.message = message.message;
    this.dateSending = message.dateSending;
};

Message.getList = (user1, user2, result) => {

    sql.query(`SELECT message.id, userSending, message,dateSending FROM message,conversation WHERE conversation.id = idConversation AND ((user1 = ${user1} AND USER2 = ${user2}) OR (user1 = ${user2} AND USER2 = ${user1})) ORDER BY dateSending, message.id`, (err, res) => {
        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res);
            return;
        }

        // not found Customer with the id
        result(null, []);

    })
}

Message.addMessage = (message, result) => {

    sql.query(`INSERT INTO message
               SET ?`, message, (err, res) => {

        if (err) {
            console.log("error: ", err);
            result(err);
            return;
        }

        result(null);
    })
}

module.exports = Message;