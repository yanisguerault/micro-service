const mysql = require("mysql");
const dbConfig = require("../config/env.config");

this.getConnection = function() {
    // Test connection health before returning it to caller.
    if ((module.exports.connection) && (module.exports.connection._socket)
        && (module.exports.connection._socket.readable)
        && (module.exports.connection._socket.writable)) {
        return module.exports.connection;
    }
    console.error(((module.exports.connection) ?
        "UNHEALTHY SQL CONNECTION; RE" : "") + "CONNECTING TO SQL.");
    var connection = mysql.createConnection({
        host     : dbConfig.HOST,
        user     : dbConfig.USER,
        password : dbConfig.PASSWORD,
        database : dbConfig.DB,
        port     : 3306
    });
    connection.connect(function(err) {
        if (err) {
            console.error("SQL CONNECT ERROR: " + err);
        } else {
            console.log("SQL CONNECT SUCCESSFUL.");
        }
    });

    connection.on("close", function (err) {
        console.log("SQL CONNECTION CLOSED.");
    });

    connection.on("error", function (err) {
        console.error("SQL CONNECTION ERROR: " + err);
    });

    module.exports.connection = connection;
    return module.exports.connection;
}

module.exports = this.getConnection();