const sql = require("./db.model");

// constructor
const Conversation = function (conversation) {
    this.id = conversation.id;
    this.user1 = conversation.user1;
    this.user2 = conversation.user2;
};

const ConversationWithoutId = function (conversation) {
    this.user1 = conversation.user1;
    this.user2 = conversation.user2;
};

Conversation.getConversationId = (user1, user2, result) => {

    sql.query(`SELECT id FROM conversation WHERE user1 = '${user1}' AND user2='${user2}'`, (err, res) => {

        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result(null, null);
    })
}

Conversation.addConversation = (conversation, result) => {
    var conv = new ConversationWithoutId(conversation)

    sql.query(`INSERT INTO conversation
               SET ?`, conv, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        result(null, res.insertId);
    })
}

module.exports = Conversation;