const Friend = require("../models/friend.model");
const axios = require('axios');
var jwt = require('jsonwebtoken');
const envConf = require("../config/env.config");

exports.getList = (req, res, next, result) => {

    var myProfile = getProfileId(req, result);

    if (myProfile == null) {
        result(false, null)
        return;
    }


    Friend.getList(myProfile, (err, data) => {
        if (err) {

            result(false, null)

        } else if (data.length) {

            result(true, data)

        } else {

            result(true, [])

        }
    });
};

exports.getUnfriendList = (req, res, next, result) => {

    var myProfile = getProfileId(req, result);

    if (myProfile == null) {
        result(false, null)
        return;
    }

    Friend.getUnfriendList(myProfile, (err, data) => {
        if (err) {

            result(false, null)

        } else if (data.length) {

            result(true, data)

        } else {

            result(true, "[]")

        }
    });
};

getProfileId = (req) => {
    var myProfile;

    if (req.body.id != undefined) {

        myProfile = req.body.id;

    } else {

        jwt.verify(req.cookies['X-Auth-Token'], envConf.JWT_KEY, (err, decoded) => {
            myProfile = decoded.profileId;
        })

    }

    if (myProfile == undefined) {
        return null;
    }

    return myProfile;
}

exports.getFriendsProfile = (req, res, next, result) => {

    this.getList(req, res, next, (value, data) => {
        if (value) {

            if (data == []) {
                result(true, data);
                return;
            }

            var entries = []

            data.forEach((element, index) => {
                axios.post("http://" + process.env.PROFILE_API + "/profile/getProfile",
                    {
                        id: element.friend2,
                        getById: true
                    }, {
                        withCredentials: true,
                        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}
                    })
                    .then(response => {

                        if (response.data == "KO") {

                            console.log("KO");
                            result(false, null);
                            return;

                        }

                        entries.push(response.data)

                        if (index === data.length - 1) {

                            callBackProfile(entries, result)
                            return;

                        }
                    })
                    .catch(e => {

                        console.log(e)
                        result(false, null)
                        return;

                    })
            });

        } else {
            result(value, data);
        }

    })
};

callBackProfile = (data, result) => {
    result(true, data);
};

exports.getNoFriendsProfile = (req, res, next, result) => {

    this.getList(req, res, next, (value, data) => {

        if (value) {

            if (data == []) {
                result(true, data);
                return;
            }

            axios.post("http://" + process.env.PROFILE_API + "/profile/getNoFriendsProfile",
                {
                    data: data,
                    idProfile: getProfileId(req)
                }, {withCredentials: true, headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}})
                .then(response => {

                    if (response.data != "KO") {
                        result(true, response.data)
                    }

                })
                .catch(e => {

                    console.log(e)
                    result(false, null)
                    return;

                })

        } else {

            result(value, data);

        }

    })
};

exports.addFriend = (req, res, next, result) => {

    var myProfile = getProfileId(req);
    var friendProfile = req.body.friend;

    if (myProfile === undefined || friendProfile === undefined) {
        result(false)
        return;
    }

    const friend = new Friend({
        friend1: myProfile,
        friend2: friendProfile,
        dateFriendship: require('moment')().format('YYYY-MM-DD')
    })


    Friend.addFriend(friend, (err) => {
        if (err) {
            result(false)
        } else {
            result(true)
        }
    });
};

exports.removeFriend = (req, res, next, result) => {

    var myProfile = getProfileId(req);
    var friendProfile = req.body.friend;

    Friend.removeFriend(myProfile, friendProfile, (err, data) => {
        if (err) {
            result(false)
        } else {
            result(true)
        }
    });
};