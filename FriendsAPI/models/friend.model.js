const sql = require("./db.model");

// constructor
const Friend = function (friend) {
    this.friend1 = friend.friend1;
    this.friend2 = friend.friend2;
    this.dateFriendship = friend.dateFriendship;
};

Friend.getList = (friend, result) => {

    sql.query(`SELECT * FROM friend WHERE friend1 = '${friend}'`, (err, res) => {
        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res);
            return;
        }

        // not found Customer with the id
        result(null, []);
    })

}

Friend.getUnfriendList = (friend, result) => {

    sql.query(`SELECT DISTINCT friend1 FROM friend WHERE friend1 != '${friend}'`, (err, res) => {
        if (err) {
            console.error("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res);
            return;
        }

        // not found Customer with the id
        result({kind: "not_found"}, false);
    })

}

Friend.addFriend = (friend, result) => {

    sql.query(`INSERT INTO friend SET ?`, friend, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err);
            return;
        }

        result(null);
    })

}

Friend.removeFriend = (friend1, friend2, result) => {

    sql.query(`DELETE FROM friend WHERE friend1='${friend1}' AND friend2='${friend2}'`, (err, res) => {
        console.log(`DELETE FROM friend WHERE friend1='${friend1}' AND friend2='${friend2}`)
        if (err) {
            console.log("error: ", err);
            result(err);
            return;
        }

        result(null);
    })

}

module.exports = Friend;