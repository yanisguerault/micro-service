module.exports = app => {
    const Friend = require("../controllers/friend.controller");

    app.post("/friends/addFriend", (req, res, next) => {

        Friend.addFriend(req, res, next, (result) =>
        {
            if(result)
            {
                res.json("OK")
            }
            else
            {
                res.json("KO FROM FRIENDS /addFriends")
            }
        });

    });

    app.post("/friends/removeFriend", (req, res, next) => {

        Friend.removeFriend(req, res, next, (result) =>
        {
            if(result)
            {
                res.json("OK");
            }
            else
            {
                res.json("KO FROM FRIENDS /removeFriend")
                console.log("KO FROM FRIENDS /removeFriend")
            }
        });

    });

    app.get("/friends/list", (req, res, next) => {

        Friend.getList(req, res, next, (result, data) =>
        {
            if(result)
            {
                res.json(data);
            }
            else
            {
                res.json("KO FROM FRIENDS /list")
                console.log("KO FROM FRIENDS /list")
            }
        });

    });

    app.get("/friends/getFriendsProfile", (req, res, next) => {

        Friend.getFriendsProfile(req, res, next, (result, data) =>
        {
            if(result)
            {
                res.json(data);
            }
            else
            {
                res.json("KO FROM FRIENDS /getFriendsProfile")
                console.log("KO FROM FRIENDS /getFriendsProfile")
            }
        });

    });

    app.get("/friends/getNoFriendsProfile", (req, res, next) => {

        Friend.getNoFriendsProfile(req, res, next, (result, data) =>
        {
            if(result)
            {
                res.json(data);
            }
            else
            {
                res.json("KO FROM FRIENDS /getNoFriendsProfile")
                console.log("KO FROM FRIENDS /getNoFriendsProfile")
            }
        });

    });

};