# Projet micro-service Guérault - Veaux - Blanquet

Lancement du projet :
`docker-compose up -d --build`

Normalement le projet se lance de façon autonome, attention cependant le premier lancement est long du a la mise en place de la BDD

Le projet contient un reverse-proxy **Traefik** qui génère toutes les routes accessibles depuis le navigateur.

Traefik est accessible sur le port **8080** et le site sur le port **80**

Chaque API est disponible avec un chemin différent (/auth, /friends, /messaging, /profile)

Les connexions sont maintenus à l'aide d'un **token JWT** et le hashage du mot de passe est en **SHA-512**.

La sécurité des API se fait à l'aide d'un **middleware traefik**, qui va vérifier auprès du service d'authentification la validité du token.

En cas de problème au lancement de la bdd, merci d'inclure le fichier `init.sql` dans la BDD.

Les différentes API sont réalisées en **NodeJs** avec le module **Express**, les différentes requêtes inter-api se font via le module **Axios**.

Les APIs sont en **REST**.

Le front est réalisé en **VueJS** utilisant également le module **Axios** pour les appels aux différentes APIs.

## Features

- Connexion/inscription/deconnexion à l'application (Auth/Profile)
- Affichage des informations du profil (Profile)
- Ajout/suppresion d'amis (Friends)
- Envoi de messages à un ami (Messagins/Friends)
- Consulation de plusieurs conversations différentes (Messaging/Friends/Profile)

# Accès

3 comptes sont disponibles par défaut, avec des amitiés et messages pré-envoyés.

- ginette@gmail.com
- wall-e@esiee.fr
- yanisguerault@gmail.com

Tous ces comptes ont pour mot de passe : **root**



**Schéma de l'architecture**

[![Schéma architecture](https://i.imgur.com/IimJMeB.png)](https://i.imgur.com/IimJMeB.png)

**Schéma des bases de données**

[![Schéma MCD](https://i.ibb.co/Q63D2NC/MCD.jpg)](https://i.ibb.co/Q63D2NC/MCD.jpg)